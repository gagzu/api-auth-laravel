<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use BaconQrCode\Writer;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;

class AuthController extends Controller {
	private $guardModel = 'api';
	private $window2FA = 8; // 8 keys (respectively 4 minutes) past and future

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	**/
	public function __construct() {}

	/**
	 * Get a JWT via given credentials.
	 *
	 * @return \Illuminate\Http\JsonResponse
	**/
	public function login(Request $request) {
		$secret2FA = $request->secret2FA;
		if($secret2FA) {
			$responseCode = 400;
			$responseData = [
				'success' => false,
				'message' => 'Secret number is not valid!',
			];

			$user = null;
			$token = null;
			$user = User::find($request->userId);

			if($user) {
				$google2fa = new Google2FA();

				$isValid = $google2fa->verifyKey(
					$user->google2fa_secret,
					(string) $secret2FA,
					$this->window2FA
				);

				if($isValid) {
					$token = $this->guard()->login($user);
				}
			}

			if($token) {
				$user->remember_token = $token;
				$user->expires_token = Carbon::now()->addHours(1);
				$user->save();

				$responseCode = 200;
				$responseData['success'] = true;
				$responseData['message'] = 'You logged in successfully';
				$responseData['data'] = [
					'user' => $user,
					'token_type' => 'bearer',
					'access_token' => $token,
					'expires_in' => $this->guard()->factory()->getTTL() * 60,
				];
			}

			return response()->json($responseData, $responseCode);
		}

		$credentials = $request->only(['email', 'password']);

		if ($token = $this->guard()->attempt($credentials)) {
			$user = $this->guard()->user();
			$user->remember_token = $token;
			$user->expires_token = Carbon::now()->addHours(1);
			$user->save();

			$data = [
				'success' => true,
				'message' => 'successful verification should now proceed to 2FA',
				'data' => [
					'user' => $user,
				]
			];

			if(!$user->google2fa_enabled) {
				$data['data']['token_type'] = 'bearer';
				$data['data']['access_token'] = $token;
				$data['message'] = 'You logged in successfully';
				$data['data']['expires_in'] = $this->guard()->factory()->getTTL() * 60;
			}

			return response()->json($data);
		} else {
			return response()->json([
				'success' => false,
				'message' => 'Some credentials are incorrect'
			], 400);
		}
	}

	/**
	 * Get the authenticated User.
	 *
	 * @return \Illuminate\Http\JsonResponse
	**/
	public function me() {
		return response()->json(auth()->user());
	}

	/**
	 * Log the user out (Invalidate the token).
	 *
	 * @return \Illuminate\Http\JsonResponse
	**/
	public function logout() {
		$this->guard()->logout();

		return response()->json(['message' => 'Successfully logged out']);
	}

	protected function guard() {
		return Auth::guard($this->guardModel);
	}
}