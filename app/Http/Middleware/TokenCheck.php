<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class TokenCheck {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	**/
	public function handle($request, Closure $next) {
		$expires = 0;
		$user = auth()->user();

		if($user) {
			$diff = Carbon::now()
							->diffInRealSeconds($user->expires_token, false);

			if($diff > 0) {
				$expires = $diff;
			} else {
				return response()->json([
					'msg' => "Token expired",
					'success' => false,
				], 403, [
					'Expires' => $expires,
					'expires_in' => $expires,
				]);
			}

			$handle = $next($request);

			if(method_exists($handle, 'header')) {
				return $handle->header('expires_in', $expires);
			} else if(method_exists($handle, 'headers')) {
				return $handle->headers->set('expires_in', $expires);
			}

			return $handle;
		} else {
			$response = (object) [
				'success' => false,
				'msg' => "Token not available",
			];
			return response()->json($response, 403);
		}
	}
}
