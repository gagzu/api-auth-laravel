# API Auth Laravel

El proposito de este repositorio es tener una base para crear una API Rest Full ya con un sistema de login implementado, y poder comenzar a trabajar directamente ya en la lógica de negocio y los end-points que requieras para tu proyecto

### ¿Comó ejecutar?

- clonar el repositorio

```
git clone git@gitlab.com:gagzu/api-auth-laravel.git
cd api-auth-laravel
```

- Instalar las dependencias:

```
composer install
```

- crear un fichero en la raíz del proyecto que se llame `.env` y copiar todo lo que hay en `.env.example` luego escribir las credenciales de la DB

```
...

DB_CONNECTION=mysql
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

...
```

- Luego de configurar en el fichero `.env` las credenciales de la DB hace falta generar clave secreta para JWT utilice el siguiente comando:

```
php artisan jwt:secret
```

Esto actualizará su `.env` archivo con algo como `JWT_SECRET=vZFaGhzyUQGHcDutsVDNLjvAua9GTjLf9uvfLJD8UKOt0l93cpl9iGtZHrnDoTmG`


- ejecutar las migraciones y los seeds de prueba

```
php artisan migrate
php artisan db:seed
```

- Hacer anda la app

```
php artisan serve
```

- Probar que todo funciona correctamente yendo a postman e importando la colección y las variables de entornos (de postman) que encontrará en la carpeta `postman` dentro del proyecto

- Una vez en postman prueba hacer login, las credenciales de ejemplo se encientran ya configuradas en la colleción que importamos

**Feliz codificación** ✌💻