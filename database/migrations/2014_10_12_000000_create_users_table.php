<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	**/
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('email')->unique();
			$table->timestamp('email_verified_at')->nullable();
			$table->boolean('enable')->default(false);
			$table->string('google2fa_secret')->nullable();
			$table->boolean('google2fa_enabled')->default(false);
			$table->dateTime('expires_token')->nullable();
			$table->string('remember_token', 512)->nullable();
			$table->string('password');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	**/
	public function down() {
		Schema::dropIfExists('users');
	}
}
